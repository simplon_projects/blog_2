<?php

namespace App\Controller;

use App\Entity\Post;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{
   /**
    * @var PostRepository
    */
   private $repository;

   public function __construct(PostRepository $repository)
    {
      $this->repository = $repository;
   }

   /**
    * @Route("/posts", name="posts.index")
    * @return Response
    */
   public function index(): Response
   {
   
      return $this->render('posts/index.html.twig');
   }

   /**
    * @route("/posts/{slug}.{id}", name="post.show", requirements={"slug": "[a-z0-9\-]*"})
    * @return Response
    */
    public function show(Post $post, string $slug): Response
    {
      if($post->getSlug() !== $slug){
         $this->redirectToRoute('post.show', [
            'id' => $post->getId(),
            'slug' => $post->getSlug()
         ], 301); 
      }
      return $this->render('posts/show.html.twig', [
         'post' => $post
      ]);
    }
}
