<?php

namespace App\Controller\Admin;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class AdminPostController extends AbstractController
{

    public function __construct(PostRepository $repository, ObjectManager $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/admin", name="admin/posts/index")
     * @return \Symphony\Component\HttpFoundation\Response
     */
    public function index(): Response
    {
        $post = $this->repository->findAll();
        return $this->render('admin/posts/index.html.twig', [
            'post' => $post
        ]);
    }

    /**
     * @Route ("/admin/post/create", name="admin.post.new")
     */
    public function new(Request $request)
    {
        $post = new Post();
        $formPost = $this->createForm(PostType::class, $post);
        $formPost->handleRequest($request);
        
        if ($formPost->isSubmitted() && $formPost->isValid()) {
            
            $this->em->persist($post);
            $this->em->flush();
            return $this->redirectToRoute('admin/posts/index');
        }
        return $this->render('admin/posts/edit.html.twig', [
            'post' => $post,
            'form' => $formPost->createView()
        ]);
    }

    /**
     * @Route("/admin/posts/{id}", name="admin.posts.edit")
     */
    public function edit(Post $post, Request $request)
    {
        $formPost = $this->createForm(PostType::class, $post);
        $formPost->handleRequest($request);
        
        if ($formPost->isSubmitted() && $formPost->isValid()) {
            $this->em->flush();
            return $this->redirectToRoute('admin/posts/index');
        }

        return $this->render('admin/posts/edit.html.twig', [
            'post' => $post,
            'form' => $formPost->createView()
        ]);
    }

}   